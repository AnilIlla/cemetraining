package com.allstate.entities;

public class Person {
    private int id;
    private String name;
    private int age;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
    public void display() {
        System.out.printf("values are %s, %d", this.name,this.id);
    }
}
