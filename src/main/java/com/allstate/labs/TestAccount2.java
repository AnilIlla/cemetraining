package com.allstate.labs;

public class TestAccount2 {
    public static void main(String[] args) {
    Account[] arrayAccounts = new Account[5];
    double[] amounts= {20,7532,6547,32,561,2132};
    String[] names = {"anil","illa","kumar","vishnu","chandra"};
    Account.setInterestRate(20);
    for (int i = 0; i < arrayAccounts.length; i++) {
        arrayAccounts[i] = new Account(names[i],amounts[i]);
        System.out.println(arrayAccounts[i].getName()+ " account has money before adding interest : "+ arrayAccounts[i].getBalance());
        arrayAccounts[i].addInterest();
        
        System.out.println(arrayAccounts[i].getName()+ " account has money after adding interest : "+ arrayAccounts[i].getBalance());
        arrayAccounts[i].withdraw(50);
        arrayAccounts[i].withdraw();
    }
    }
}
    
