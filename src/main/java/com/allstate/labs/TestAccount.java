package com.allstate.labs;

public class TestAccount {
    public static void main(String[] args) {
        Account account = new Account();
        account.setBalance(500);
        account.setName("anil");
        System.out.println(account.getName()+ " account has money before adding interest : "+ account.getBalance());
        account.addInterest();
        System.out.println(account.getName()+ " account has money after adding interest : "+ account.getBalance());
        Account[] arrayAccounts = new Account[5];
        double[] amounts= {230,7532,6547,32,561,2132};
        String[] names = {"anil","illa","kumar","vishnu","chandra"};
        for (int i = 0; i < arrayAccounts.length; i++) {
            arrayAccounts[i] = new Account();
            arrayAccounts[i].setBalance(amounts[i]);
            arrayAccounts[i].setName(names[i]);
            System.out.println(arrayAccounts[i].getName()+ " account has money before adding interest : "+ arrayAccounts[i].getBalance());
            arrayAccounts[i].addInterest();
            System.out.println(arrayAccounts[i].getName()+ " account has money after adding interest : "+ arrayAccounts[i].getBalance());
        }
    }
    
}
