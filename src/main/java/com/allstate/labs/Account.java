package com.allstate.labs;

public class Account {
    private double balance;
    private String name;
    private static double interestRate;

    public Account() {
        this.name="illa";
        this.balance = 50;
    }

    public Account(String name,double balance) {
        this.name=name;
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public void addInterest() {
        this.balance = this.balance+(this.balance*(interestRate/100));
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public boolean withdraw(double amount) {
        if (this.balance>=amount) {
            this.balance = this.balance-amount;
            System.out.println("successfully withdrawn money from your account");
            return true;
        }
        System.out.println("sorry your transaction failed because of insufficent balance");
        return false;
    }
    public boolean withdraw() {
        return withdraw(20);
    }
    
}
